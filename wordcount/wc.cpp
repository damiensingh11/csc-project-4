/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 10+
 */
#include<iostream>
using std::cout;
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

set<string> UNIQUE;

size_t sfind(const string s) //takes string and counts
{
  string a;

int linecount = 0;
int state = 0; // 0 for whtespce or tab----1 for letter
  size_t count =  0; //word count
  for(size_t i = 0; i < s.length(); i++ ) { //go thru string
      a+=s[i-1];   //add element to string
      if (state == 0) {

      if (s[i-1]!= ' ' && s[i-1]!= '\t') { //we didnt read space
        count++;
        state = 1;


      }

    }
    else if(s[i] == ' ' || s[i] == '\t' || s[i-1] == '\n') {

      state = 0;
      UNIQUE.insert(a);  //insert
      a.clear();         //reset
}
    if (s[i-1] == '\n') {

    linecount++;

  }
  }

  for(set<string>::iterator i = UNIQUE.begin(); i!=UNIQUE.end(); i++) {
   cout<< *i <<'\n';
  }

  cout << linecount;
  return count;
}

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
char p;
  string s;
  while (fread(&p,1,1,stdin)) {
        s += p;
  }

cout << "  " << sfind(s) << "   " << s.length() << "  " << '\n';
  cout << "unique " << UNIQUE.size() << '\n';

	return 0;
}
